var data = {};
data.dsm = {
    top: {
        title: 'CBCL/6-18 DSM-georienteerde schalen - Meisjes 6-11',
        naam: 'Liesje van',
        geboorteDatum: '17-03-1996',
        invulDatum: '20-02-2008',
        invuldDoor: '20-02-2008',
        meetmoment: 'Start',
        geslacht: 'Vrouw',
        leeftijd: '11 jaar',
        naamInvuller: ''

    },
    naam: 'Liesje van Heuvelen',
    geboortedatum: '17-03-1996',
    rightLabel: {
        top: 'Klinisch',
        bottom: 'Normaal'
    },
    scoreTypes: {raw: 'Ruwe Score', total: 'T Score', percentile: 'Percentiel'},
    tableCaption: 'Onderbroken lijnen geven de grenzen van het klinisch bereik aan <br> Scoring gebaseerd op de ' +
    'multicultureele normen voor Groep 2 (NL, VS; Achenbach & Rescola  2007).<br> ' +
    '`nb`: niet berekend wegens ontbrekende gegevens.' +
    '<br><br> &copy; T.M Achenbach. Reproduced by permission under License Number 27469-0110.',
    dashLines: {y1: 75, y2: 60},
    type: [
        {
            name: 'Affectieve Problemen', scores: {raw: 13, total: 79, percentile: 100},
            lists: [
                {key: 1, value: '5. Weinig leuk'},
                {key: 1, value: '14. Huilt veel'},
                {key: 0, value: '18. Verwondt zichzelf'},
                {key: 0, value: '18. Verwondt zichzelf'},
                {key: 0, value: '18. Verwondt zichzelf'}
            ]
        }, {
            name: 'Angstproblemen', scores: {raw: 6, total: 70, percentile: 98},
            lists: [
                {key: 1, value: '5. Weinig leuk'},
                {key: 1, value: '14. Huilt veel'},
                {key: 0, value: '18. Verwondt zichzelf'}
            ]
        }, {
            name: 'Somatische Problemen', scores: {raw: 6, total: 73, percentile: 99},
            lists: [
                {key: 1, value: '5. Weinig leuk'},
                {key: 1, value: '14. Huilt veel'},
                {key: 0, value: '18. Verwondt zichzelf'}
            ]
        }, {
            name: 'Hyperactiviteits problemen', scores: {raw: 5, total: 56, percentile: 73},
            lists: [
                {key: 1, value: '5. Weinig leuk'},
                {key: 1, value: '14. Huilt veel'},
                {key: 0, value: '18. Verwondt zichzelf'}
            ]
        }, {
            name: 'Oppositioneel', scores: {raw: 6, total: 66, percentile: 95},
            lists: [
                {key: 1, value: '5. Weinig leuk'},
                {key: 1, value: '14. Huilt veel'},
                {key: 0, value: '18. Verwondt zichzelf'}
            ]
        }, {
            name: 'Hyperactiviteits problemen', scores: {raw: 5, total: 56, percentile: 73},
            lists: [
                {key: 1, value: '5. Weinig leuk'},
                {key: 1, value: '14. Huilt veel'},
                {key: 0, value: '18. Verwondt zichzelf'}
            ]
        }
    ]
};
data.total = {
    top: {
        title: 'CBCL/6-18 DSM-georienteerde schalen - Meisjes 6-11',
        naam: 'Liesje van',
        geboorteDatum: '17-03-1996',
        invulDatum: '20-02-2008',
        invuldDoor: '20-02-2008',
        meetmoment: 'Start',
        geslacht: 'Vrouw',
        leeftijd: '11 jaar',
        naamInvuller: ''

    },
    naam: 'Liesje van Heuvelen',
    geboortedatum: '17-03-1996',
    rightLabel: {
        top: 'Klinisch',
        bottom: 'Normaal'
    },
    scoreTypes: {raw: 'Ruwe Score', total: 'T Score', percentile: 'Percentiel'},
    tableCaption: 'Onderbroken lijnen geven de grenzen van het klinisch bereik aan <br> Scoring gebaseerd op de ' +
    'multicultureele normen voor Groep 2 (NL, VS; Achenbach & Rescola  2007).<br> ' +
    '`nb`: niet berekend wegens ontbrekende gegevens.' +
    '<br><br> &copy; T.M Achenbach. Reproduced by permission under License Number 27469-0110.',
    dashLines: {y1: 74, y2: 65},
    type: [
        {
            name: 'Internaliseren', scores: {total: 77, raw: 30, percentile: 100}
        }, {
            name: 'Externaliseren', scores: {total: 75, raw: 32, percentile: 99}
        }, {
            name: 'Totale Problemen', scores: {total: 78, raw: 110, percentile: 100}
        }
    ]
};
data.competentie = {
    top: {
        title: 'CBCL/6-18 Competentieschalen - Meisjes 6-11',
        naam: 'Liesje van',
        geboorteDatum: '17-03-1996',
        invulDatum: '20-02-2008',
        invuldDoor: '20-02-2008',
        meetmoment: 'Start',
        geslacht: 'Vrouw',
        leeftijd: '11 jaar',
        naamInvuller: ''

    },
    naam: 'Liesje van Heuvelen',
    geboortedatum: '17-03-1996',
    rightLabel: {
        top: 'Klinisch',
        bottom: 'Normaal'
    },
    scoreTypes: {raw: 'Ruwe Score', total: 'T Score', percentile: 'Percentiel'},
    tableCaption: 'Onderbroken lijnen geven de grenzen van het klinisch bereik aan <br> Scoring gebaseerd op de ' +
    'multicultureele normen voor Groep 2 (NL, VS; Achenbach & Rescola  2007).<br> ' +
    '`nb`: niet berekend wegens ontbrekende gegevens.' +
    '<br><br> &copy; T.M Achenbach. Reproduced by permission under License Number 27469-0110.',
    dashLines: {y1: 30, y2: 45},
    type: [
        {
            name: 'Activiteiten', scores: {raw: 13, total: 79, percentile: 100},
            lists: [
                {key: 1, value: '5. Weinig leuk'},
                {key: 1, value: '14. Huit veel'},
                {key: 0, value: '18. Verwondt zichzelf'}
            ]
        }, {
            name: 'Sociaal', scores: {raw: 6, total: 70, percentile: 98},
            lists: [
                {key: 1, value: '5. Weinig leuk'},
                {key: 1, value: '14. Huit veel'},
                {key: 0, value: '18. Verwondt zichzelf'}
            ]
        }, {
            name: 'School', scores: {raw: 6, total: 73, percentile: 99},
            lists: [
                {key: 1, value: '5. Weinig leuk'},
                {key: 1, value: '14. Huit veel'},
                {key: 0, value: '18. Verwondt zichzelf'}
            ]
        }
    ],
    right: {
        dashLines: {y1: 30, y2: 40},
        type: [
            {
                name: 'Totale Vaardigheden', scores: {raw: 10, total: 20, percentile: 15},
                lists: []
            }

        ]
    }
};
reports = {};