jQuery(function () {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var id = $(e.target).attr("href").substr(1);
        if (e.target.getAttribute('data-called') !== "true") {
            e.target.setAttribute('data-called', true);
            reports[id].init();
        }
        var scoreType = $('#' + id).attr('data-score-type');
        var button = $('#btn-score').find('[data-score-type="' + scoreType + '"]');
        button.siblings().removeClass('active');
        button.addClass('active');
    });
    reports.dsm.init();
    $('#btn-score').find('button').on('click', function () {
        var id = $('.tab-pane.active').first().attr('id');
        var scoreType = $(this).attr('data-score-type');
        reports[id].id = id;
        reports[id].scoreType = scoreType;
        reports[id].init();
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $('#' + id).attr('data-score-type', scoreType);
    });
});