/**
 * Created by tunu on 10/20/2015.
 */
var reportsPrototype = {
        chartHeight: 500,
        MARGINS: {top: 90, right: 20, bottom: 20, left: 100},
        extraMethods: [],
        init: function () {
            this.WIDTH = 0;
            this.HEIGHT = 0;
            d3.select("#" + this.id + "-container").html('');
            this.svgContainer = d3.select("#" + this.id + "-container")
                .append("svg")
                .attr('width', '100%');

            this.chart = this.svgContainer.append('g')
                .attr('transform', 'translate(' + this.MARGINS.left + ',' + this.MARGINS.top + ')')
                .attr('class', 'chart');

            //Update Height and Width
            this.addToWidth(this.MARGINS.left);

            this.config();
        }
        ,
        config: function () {
            this.setData(data[this.id]);
            this.drawTopParagraph();
            this.drawXAxis();
            this.drawYAxis();
            this.drawChart();
            this.drawRightLabels();
            this.drawTable();
            this.drawRightTable();
            this.drawDashedLines();
            this.finish();
        }
        ,
        drawChart: function () {
            this[this.chartMethod]();
        },
        setData: function (data) {
            this.data = jQuery.extend(true, {}, data);
            this.addToWidth(this.data.type.length * 200);
        }
        ,
        addToWidth: function (newWidth) {
            this.WIDTH += newWidth;
        },
        addToHeight: function (newHeight) {
            this.HEIGHT += newHeight;
        },
        drawTopParagraph: function () {
            this.svgContainer.append('foreignObject')
                .attr('transform', 'translate(' + this.MARGINS.left + ',10)')
                .attr('class', 'top-paragraph')
                .attr('height', '80px')
                .html(this.getTopParagraphHTML());
            this.addToHeight(90);
        }
        ,
        getTopParagraphHTML: function () {
            return '<div class="container">' +
                '<h5><strong>' + this.data.top.title + '</strong></h5>' +
                '<div class="row">' +
                '<div class="col-xs-2">Naam: ' + this.data.top.naam +
                '</div><div class="col-xs-2">Geboortedatum: ' + this.data.top.geboorteDatum +
                '</div><div class="col-xs-2">Invuldatum: ' + this.data.top.invulDatum +
                '</div><div class="col-xs-2">Ingevuld door: ' + this.data.top.invuldDoor +
                '</div><div class="col-xs-2">Meetmoment(en): ' + this.data.top.meetmoment +
                '</div></div><div class="row">' +
                '<div class="col-xs-2">Geslacht: ' + this.data.top.geslacht +
                '</div><div class="col-xs-2">Leeftijd: ' + this.data.top.leeftijd +
                '</div><div class="col-xs-2">Naam invuller:' + this.data.top.naamInvuller +
                '</div></div></div>';
        }
        ,
        drawXAxis: function () {
            this.xScale = d3.scale.ordinal()
                .rangeRoundBands([0, this.WIDTH - this.MARGINS.left])
                .domain(this.data.type.map(function (d) {
                    return d.name;
                }));

            this.xAxis = d3.svg.axis()
                .scale(this.xScale)
                .tickSize(0)
                .tickFormat('')
                .orient('bottom');

            this.chart.append('g')
                .attr("class", "x axis")
                .attr("transform", "translate(0," + (this.chartHeight - this.MARGINS.top - 10) + ")")
                .call(this.xAxis);

            for (var index = 0; index <= this.data.type.length; index++) {
                this.chart.append('line')
                    .style('stroke', 'black')
                    .style('stroke-width', 2)
                    .attr('x1', Math.floor(this.xScale.rangeBand()) * index)
                    .attr('x2', Math.floor(this.xScale.rangeBand()) * index)
                    .attr('y1', 0)
                    .attr('y2', this.chart.node().getBoundingClientRect().height);
            }
        }
        ,
        drawYAxis: function () {
            var self = this;
            var maximum = d3.max(this.data.type, function (d) {
                    return d.scores[self.scoreType];
                }) + 10;
            maximum = Math.max(maximum, 100);
            this.yScale = d3.scale.linear()
                .rangeRound([this.chartHeight - this.MARGINS.left, 0])
                .domain([0, maximum]);

            this.yAxis = d3.svg.axis()
                .scale(this.yScale)
                .tickSize(-Math.floor(this.xScale.rangeBand()) * this.data.type.length, 10, 1)
                .ticks(20)
                .orient('left');

            this.chart.append('g')
                .attr("class", "y axis")
                .call(this.yAxis);

            //y axis left label
            this.svgContainer.append('text')
                .attr('x', 0)
                .attr('y', this.chartHeight / 2)
                .attr('text-anchor', 'end')
                .style('font-weight', 'bold')
                .attr('fill', 'steelblue')
                .attr('transform', 'rotate(-90,20,' + (this.chartHeight / 2 - 50) + ')')
                .text(this.data.scoreTypes[this.scoreType]);
            this.addToHeight(this.chart.node().getBoundingClientRect().height);
        }
        ,
        drawRightLabels: function () {
            this.rightLabelGroup = this.chart.append('g')
                .attr('transform', 'translate(' + (this.chart.node().getBoundingClientRect().width - 10) + ',0)')
                .attr('glyph-orientation-vertical', "0")
                .style('font-weight', 'bold')
                .attr('writing-mode', "tb-rl");

            this.rightLabelGroup.append('text').text(this.data.rightLabel.top);

            this.rightLabelGroup.append('text')
                .attr('transform', 'translate(0,' + (this.chart.node().getBoundingClientRect().height - 25) + ')')
                .attr('text-anchor', 'end')
                .text(this.data.rightLabel.bottom);
            this.addToWidth(30)
        }
        ,
        finish: function () {
            this.svgContainer
                .attr('height', this.HEIGHT + 'px')
                .attr('width', this.WIDTH + 'px');
        },
        drawTable: function () {
            var table, tbody, self = this;
            this.data.type.unshift({name: "", scores: {raw: this.data.scoreTypes.raw, total: this.data.scoreTypes.total, percentile: this.data.scoreTypes.percentile}, lists: []});
            table = document.createElement('table');
            table = d3.select(table);
            table.append('thead')
                .append('tr')
                .selectAll('th')
                .data(this.data.type)
                .enter()
                .append('th')
                .attr('style', "width: " + (Math.floor(this.xScale.rangeBand()) - 18) + "px")
                .text(function (d) {
                    return d.name;
                });


            tbody = table.append("tbody");


            //Sort tab score according to chart score
            var scoreTypes = ['total', 'raw', 'percentile'];
            scoreTypes.splice(scoreTypes.indexOf(this.scoreType), 1);
            scoreTypes.unshift(this.scoreType);

            scoreTypes.forEach(function (score) {
                tbody.append('tr')
                    .selectAll('td')
                    .data(self.data.type)
                    .enter()
                    .append("td")
                    .attr('style', "width: " + (Math.floor(self.xScale.rangeBand()) - 18) + "px")
                    .text(function (d) {
                        return d.scores[score];
                    });
            });

            //The last row
            if (this.data.type[1].lists) {
                var tdList = tbody.append('tr')
                    .selectAll('td')
                    .data(this.data.type)
                    .enter()
                    .append('td');

                var row = tdList.selectAll('div')
                    .data(function (d) {
                        return d.lists;
                    })
                    .enter()
                    .append('div');
                row.append('span')
                    .attr('class', 'row-key')
                    .text(function (d) {
                        return d.key;
                    });
                row.append('span')
                    .attr('class', 'row-value')
                    .text(function (d) {
                        return d.value;
                    });
            }


            this.bottomTable = this.svgContainer.append('foreignObject')
                .attr('width', (Math.floor(this.xScale.rangeBand()) * (this.data.type.length - 1) + this.MARGINS.left + 1) + 'px')
                .attr('height', '100%')
                .attr('transform', 'translate(0,' + (this.chart.node().getBoundingClientRect().height + this.MARGINS.top - 14) + ')')
                .html('<table class="table bottom-table">' + table.html() + '</table>');

            this.bottomTable.height = d3.select('#' + this.id + '-container svg table').node().getBoundingClientRect().height;

            this.bottomTable.attr('height', this.bottomTable.height + "px");

            this.license = document.createElement('p');
            this.license = d3.select(this.license);
            this.license.append('i')
                .html(this.data.tableCaption);

            this.license = this.svgContainer.append('foreignObject')
                .attr('width', this.chart.node().getBoundingClientRect().width)
                .attr('height', '100px')
                .attr('transform', 'translate(' + this.MARGINS.left + ',' + (this.chart.node().getBoundingClientRect().height + this.MARGINS.top + this.bottomTable.height) + ')')
                .html(this.license.html());

            //License Height
            this.addToHeight(100);
            this.WIDTH = this.chart.node().getBoundingClientRect().width + this.MARGINS.left;
            this.addToHeight(this.bottomTable.height)

        }
        ,
        drawDashedLines: function () {
            var graphLineGroup = this.chart.append('g')
                .style('stroke', '#5f70b4')
                .style('stroke-dasharray', '10, 5')
                .attr('stroke-width', 2.5);

            var width = Math.floor(this.xScale.rangeBand()) * (this.data.type.length - 1) + this.MARGINS.left - 101;
            graphLineGroup.append('line')
                .attr('x1', 1)
                .attr('y1', (this.yScale(this.data.dashLines.y1)))
                .attr('x2', width)
                .attr('y2', (this.yScale(this.data.dashLines.y1)));
            graphLineGroup.append('line')
                .attr('x1', 1)
                .attr('y1', (this.yScale(this.data.dashLines.y2)))
                .attr('x2', width)
                .attr('y2', (this.yScale(this.data.dashLines.y2)));
        }
        ,
        drawLines: function () {
            var self = this;
            var valueLine = d3.svg.line()
                .x(function (d) {
                    return self.xScale(d.name);
                })
                .y(function (d) {
                    return self.yScale(d.scores[self.scoreType]);
                });
            // Add the valueline path.
            var lineGroup = this.chart.append('g')
                .attr('transform', 'translate(' + this.xScale.rangeBand() / 2 + ',0)');
            lineGroup.append("path")
                .attr("class", "line")
                .attr("d", valueLine(this.data.type));

            var squares = lineGroup.selectAll(".bar")
                .data(this.data.type)
                .enter()
                .append("g")
                .attr("class", "bar");

            //create the squares.
            squares.append("rect")
                .attr("x", function (d) {
                    return self.xScale(d.name);
                })
                .attr("y", function (d) {
                    return self.yScale(d.scores[self.scoreType]);
                })
                .attr('transform', 'translate(-2.5,-2.5)')
                .attr("width", 5)
                .attr("height", 5);
        }
        ,
        drawRightTable: function () {
        },
        drawBars: function () {
            var self = this;
            var rectGroup = this.chart.append('g')
                .attr('transform', 'translate(' + this.xScale.rangeBand() / 4 + ',0)');

            var bars = rectGroup.selectAll(".bar")
                .data(this.data.type)
                .enter()
                .append("g")
                .attr("class", "bar");

            //create the rectangles.
            bars.append("rect")
                .attr("x", function (d) {
                    return self.xScale(d.name);
                })
                .attr("y", function (d) {
                    return self.yScale(d.scores[self.scoreType]);
                })
                .attr("width", self.xScale.rangeBand() / 2)
                .attr("height", function (d) {
                    return 400 - self.yScale(d.scores[self.scoreType]);
                })
                .attr('fill', '#4a92c7');
        }
    }
    ;
