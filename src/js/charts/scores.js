/**
 * Created by tunu on 10/13/2015.
 */

reports.dsm = jQuery.extend(true, {}, reportsPrototype);
reports.dsm.id = 'dsm';
reports.dsm.scoreType = 'total';
reports.dsm.chartMethod = 'drawLines';

reports.total = jQuery.extend(true, {}, reportsPrototype);
reports.total.id = 'total';
reports.total.scoreType = 'total';
reports.total.chartMethod = 'drawBars';
reports.total.drawRightTable = function () {
    var rightTable = this.svgContainer.append('foreignObject');
    rightTable
        .attr('width', 300)
        .attr('height', this.HEIGHT - 30)
        .attr('transform', 'translate(' + ((this.chart.node().getBoundingClientRect().width + 100)) + ',' + this.MARGINS.top + ')')
        .html('<table class="table right-table"><thead>' +
        '<tr><th colspan="2" class="text-center">Andere problemen</th></tr> </thead>' +
        '<tbody><tr><td colspan="2" class="text-center">12</td></tr>' +
        '<tr><td>0</td><td><strong>6. Poept buiten wc c</strong></td></tr>' +
        '<tr><td>2</td><td>7. Schept op</td></tr>' +
        '<tr><td>0</td><td><strong>15. Wreed tegen Dieren c</strong></td></tr>' +
        '<tr><td>1</td><td>24. Eet niet good</td></tr>' +
        '<tr><td>0</td><td>44. Bijt nagels</td></tr>' +
        '<tr><td>2</td><td>53. Te dik</td></tr>' +
        '<tr><td>0</td><td>55. LichaamproblOverig</td></tr>' +
        '<tr><td>1</td><td>h. Poept buiten wc c</td></tr>' +
        '<tr><td>0</td><td>74. Zeuren</td></tr>' +
        '<tr><td>1</td><td>77. Poept buiten wc c</td></tr>' +
        '</tbody></table>' +
        '<i>VET geprinte items met een c aan het einde zijn kritische items (critical items)</i>'
    );
    this.addToWidth(340);
};

reports.competentie = jQuery.extend(true, {}, reportsPrototype);

reports.competentie.id = 'competentie';
reports.competentie.scoreType = 'total';
reports.competentie.chartMethod = 'drawBars';

reports.competentie.drawRightTable = function () {
    var self = this;
    // Y axis
    var rightChart = self.svgContainer.append('g')
        .attr('transform', 'translate(' + (self.MARGINS.left + self.chart.node().getBoundingClientRect().width + 250) + ',' + (self.MARGINS.top) + ')');

    // X axis
    var xScale = d3.scale.ordinal()
        .rangeRoundBands([0, this.WIDTH / this.data.type.length])
        .domain(self.data.right.type.map(function (d) {
            return d.name;
        }));

    var xAxis = d3.svg.axis()
        .scale(xScale)
        .tickSize(0)
        .tickFormat('')
        .orient('bottom');

    rightChart.append('g')
        .attr("class", "x axis")
        .attr("transform", "translate(0," + (400 - self.MARGINS.top) + ")")
        .call(xAxis);


    var yScale = d3.scale.linear()
        .rangeRound([400 - self.MARGINS.left, 0])
        .domain([0, 80]);

    var yAxis = d3.svg.axis()
        .scale(yScale)
        .tickSize(-xScale.rangeBand(), 10, 1)
        .ticks(20)
        .orient('left');

    rightChart.append('g')
        .attr("class", "y axis")
        .call(yAxis);

    //y axis left label
    rightChart.append('text')
        .attr('x', 0)
        .attr('y', 200)
        .attr('text-anchor', 'end')
        .style('font-weight', 'bold')
        .attr('fill', 'steelblue')
        .attr('transform', 'rotate(-90,-60,170)')
        .text(self.data.scoreTypes[self.scoreType]);


    for (var index = 0; index <= self.data.right.type.length; index++) {
        rightChart.append('line')
            .style('stroke', 'black')
            .style('stroke-width', 2)
            .attr('x1', (xScale.rangeBand()) * index)
            .attr('x2', (xScale.rangeBand()) * index)
            .attr('y1', 0)
            .attr('y2', rightChart.node().getBoundingClientRect().height - 16);
    }


    //Right Labels
    var group = rightChart.append('g')
        .attr('transform', 'translate(' + (xScale.rangeBand() + 10) + ',0)')
        .attr('glyph-orientation-vertical', "0")
        .style('font-weight', 'bold')
        .attr('writing-mode', "tb-rl");

    group.append('text')
        .text(self.data.rightLabel.bottom);

    group.append('text')
        .attr('transform', 'translate(0,' + (rightChart.node().getBoundingClientRect().height - 30) + ')')
        .attr('text-anchor', 'end')
        .text(self.data.rightLabel.top);

    //Dash lines
    var graphLineGroup = rightChart.append('g')
        .style('stroke', '#ff0000')
        .style('stroke-dasharray', '10, 5')
        .attr('stroke-width', 2.5);

    var x = xScale.rangeBand;
    graphLineGroup.append('line')
        .attr('x1', 0)
        .attr('y1', (yScale(self.data.right.dashLines.y1)))
        .attr('x2', x)
        .attr('y2', (yScale(self.data.right.dashLines.y1)));

    graphLineGroup.append('line')
        .attr('x1', 0)
        .attr('y1', (yScale(self.data.right.dashLines.y2)))
        .attr('x2', x)
        .attr('y2', (yScale(self.data.right.dashLines.y2)));

    //Table

    var table, tbody;

    table = document.createElement('table');
    table = d3.select(table);
    self.data.right.type.unshift({name: "", scores: {raw: self.data.scoreTypes.total, total: self.data.scoreTypes.raw, percentile: self.data.scoreTypes.percentile}, lists: []});
    table.append('thead')
        .append('tr')
        .selectAll('th')
        .data(this.data.right.type)
        .enter()
        .append('th')
        .attr('style', "width: " + (Math.floor(this.xScale.rangeBand()) - 18) + "px")
        .text(function (d) {
            return d.name;
        });

    tbody = table.append("tbody");

    var scoreTypes = ['raw', 'total', 'percentile'];
    scoreTypes.splice(scoreTypes.indexOf(this.scoreType), 1);
    scoreTypes.unshift(this.scoreType);

    scoreTypes.forEach(function (score) {
        tbody.append('tr')
            .selectAll('td')
            .data(self.data.right.type)
            .enter()
            .append("td")
            .attr('style', "width: " + (self.xScale.rangeBand() - 18) + "px")
            .text(function (d) {
                return d.scores[score];
            });
    });

    rightChart.append('foreignObject')
        .attr('width', (91 + xScale.rangeBand()))
        .attr('height', '100px')
        .attr('transform', 'translate(-90,300)')
        .html('<table class="table right-table bottom-table">' + table.html() + '</table>');
    this.addToWidth(500);
};